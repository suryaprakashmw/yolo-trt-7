#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "NvInfer.h"
#include "yolo.h"
#include "logger.h"

const int32_t MAX_BATCH_SIZE = 4;

const char* AD_CFG_PATH = "../Lip/Lip.cfg";
const char* AD_WTS_PATH = "../Lip/Lip.weights";

const char* ad_input_blob_name = "data";

Logger gLogger;

void giestream_to_file(nvinfer1::IHostMemory *trtModelStream, const std::string filename)
{
    assert(trtModelStream != nullptr);
    std::ofstream outfile(filename, std::ofstream::binary);
    assert(!outfile.fail());
	outfile.write(reinterpret_cast<char*>(trtModelStream->data()), trtModelStream->size());
    outfile.close();
}

nvinfer1::ICudaEngine* initEngine(const char* cfg_path, const char* weight_path, const char* input_blob_name, nvinfer1::IBuilder* builder) {
    NetworkInfo info;
    info.networkType = "yolov2-tiny";
    info.configFilePath = cfg_path;
    info.wtsFilePath = weight_path;
    info.deviceType = "kGPU";
    info.inputBlobName = input_blob_name;
    
    Yolo yolo(info, builder);
    return yolo.createEngine();
}

int32_t main(int32_t argc, char** argv) {

    nvinfer1::IBuilder* builder = nvinfer1::createInferBuilder(gLogger);
    builder->setMaxBatchSize(MAX_BATCH_SIZE);
    nvinfer1::ICudaEngine* engine = initEngine(AD_CFG_PATH, AD_WTS_PATH, ad_input_blob_name,  builder);
    nvinfer1::IHostMemory *serializedModel = engine->serialize();

    int64_t size = serializedModel->size();

    std::cerr << "size = " << size << std::endl;
    giestream_to_file(serializedModel, "../Lip/Lip.engine");
    engine->destroy();
    serializedModel->destroy();
}
